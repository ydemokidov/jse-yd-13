package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
