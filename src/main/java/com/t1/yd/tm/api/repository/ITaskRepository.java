package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    int getSize();

}
