package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    boolean existsById(String id);

    int getSize();

}