package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task add(final Task task) {
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;

        final Task task = findOneById(id);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;

        final Task task = findOneByIndex(index);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) return null;
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }
}
