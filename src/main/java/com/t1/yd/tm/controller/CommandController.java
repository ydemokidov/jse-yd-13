package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.controller.ICommandController;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.model.Command;
import com.t1.yd.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yuriy Demokidov");
        System.out.println("Email: ydemokidov@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getTerminalCommands()) {
            System.out.println(command);
        }
    }

    @Override
    public void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Argument is not supported");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Command is not supported");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");

        final Command[] commands = commandService.getTerminalCommands();

        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");

        final Command[] commands = commandService.getTerminalCommands();

        for (final Command command : commands) {
            final String arg = command.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory : " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory : " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory : " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        System.out.println("Used memory : " + usedMemoryFormat);
    }

}
