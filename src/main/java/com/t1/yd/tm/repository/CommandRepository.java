package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(ArgumentConstant.HELP, CommandConstant.HELP, "Show info about program");

    private static final Command ABOUT = new Command(ArgumentConstant.ABOUT, CommandConstant.ABOUT, "Show info about program");

    private static final Command VERSION = new Command(ArgumentConstant.VERSION, CommandConstant.VERSION, "Show program version");

    private static final Command INFO = new Command(ArgumentConstant.INFO, CommandConstant.INFO, "Show system info");

    private static final Command COMMANDS = new Command(ArgumentConstant.COMMANDS, CommandConstant.COMMANDS, "Show available commands");

    private static final Command ARGUMENTS = new Command(ArgumentConstant.ARGUMENTS, CommandConstant.ARGUMENTS, "Show available arguments");

    private static final Command PROJECT_CREATE = new Command(null, CommandConstant.PROJECT_CREATE, "Create new project");

    private static final Command PROJECT_LIST = new Command(null, CommandConstant.PROJECT_LIST, "Show all projects");

    private static final Command PROJECT_GET_BY_ID = new Command(null, CommandConstant.PROJECT_GET_BY_ID, "Show project by id");

    private static final Command PROJECT_GET_BY_INDEX = new Command(null, CommandConstant.PROJECT_GET_BY_INDEX, "Show project by index");

    private static final Command PROJECT_UPDATE_BY_ID = new Command(null, CommandConstant.PROJECT_UPDATE_BY_ID, "Update project by id");

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(null, CommandConstant.PROJECT_UPDATE_BY_INDEX, "Update project by index");

    private static final Command PROJECT_REMOVE_BY_ID = new Command(null, CommandConstant.PROJECT_REMOVE_BY_ID, "Remove project by id");

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(null, CommandConstant.PROJECT_REMOVE_BY_INDEX, "Remove project by index");

    private static final Command PROJECT_START_BY_ID = new Command(null, CommandConstant.PROJECT_START_BY_ID, "Start project by id");

    private static final Command PROJECT_START_BY_INDEX = new Command(null, CommandConstant.PROJECT_START_BY_INDEX, "Start project by index");

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(null, CommandConstant.PROJECT_COMPLETE_BY_ID, "Complete project by id");

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(null, CommandConstant.PROJECT_COMPLETE_BY_INDEX, "Complete project by index");

    private static final Command PROJECT_UPDATE_STATUS_BY_ID = new Command(null, CommandConstant.PROJECT_UPDATE_STATUS_BY_ID, "Update project status by id");

    private static final Command PROJECT_UPDATE_STATUS_BY_INDEX = new Command(null, CommandConstant.PROJECT_UPDATE_STATUS_BY_INDEX, "Update project status by index");

    private static final Command PROJECT_CLEAR = new Command(null, CommandConstant.PROJECT_CLEAR, "Clear all projects");

    private static final Command TASK_CREATE = new Command(null, CommandConstant.TASK_CREATE, "Create new task");

    private static final Command TASK_LIST = new Command(null, CommandConstant.TASK_LIST, "List all tasks");

    private static final Command TASK_GET_BY_ID = new Command(null, CommandConstant.TASK_GET_BY_ID, "Show task by id");

    private static final Command TASK_GET_BY_INDEX = new Command(null, CommandConstant.TASK_GET_BY_INDEX, "Show task by index");

    private static final Command TASK_UPDATE_BY_ID = new Command(null, CommandConstant.TASK_UPDATE_BY_ID, "Update task by id");

    private static final Command TASK_UPDATE_BY_INDEX = new Command(null, CommandConstant.TASK_UPDATE_BY_INDEX, "Update task by index");

    private static final Command TASK_REMOVE_BY_ID = new Command(null, CommandConstant.TASK_REMOVE_BY_ID, "Remove task by id");

    private static final Command TASK_REMOVE_BY_INDEX = new Command(null, CommandConstant.TASK_REMOVE_BY_INDEX, "Remove task by index");

    private static final Command TASK_START_BY_ID = new Command(null, CommandConstant.TASK_START_BY_ID, "Start task by id");

    private static final Command TASK_START_BY_INDEX = new Command(null, CommandConstant.TASK_START_BY_INDEX, "Start task by index");

    private static final Command TASK_COMPLETE_BY_ID = new Command(null, CommandConstant.TASK_COMPLETE_BY_ID, "Complete task by id");

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(null, CommandConstant.TASK_COMPLETE_BY_INDEX, "Complete task by index");

    private static final Command TASK_UPDATE_STATUS_BY_ID = new Command(null, CommandConstant.TASK_UPDATE_STATUS_BY_ID, "Update task status by id");

    private static final Command TASK_UPDATE_STATUS_BY_INDEX = new Command(null, CommandConstant.TASK_UPDATE_STATUS_BY_INDEX, "Update task status by index");

    private static final Command TASK_BIND_TO_PROJECT = new Command(null, CommandConstant.TASK_BIND_TO_PROJECT, "Bind task to project");

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(null, CommandConstant.TASK_UNBIND_FROM_PROJECT, "Unbind task from project");

    private static final Command TASK_SHOW_BY_PROJECT = new Command(null, CommandConstant.TASK_SHOW_BY_PROJECT, "Show tasks by project");

    private static final Command TASK_CLEAR = new Command(null, CommandConstant.TASK_CLEAR, "Clear all tasks");

    private static final Command EXIT = new Command(null, CommandConstant.EXIT, "Exit program");

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP,
            ABOUT,
            VERSION,
            INFO,
            COMMANDS,
            ARGUMENTS,
            PROJECT_CREATE,
            PROJECT_LIST,
            PROJECT_CLEAR,
            PROJECT_GET_BY_ID,
            PROJECT_GET_BY_INDEX,
            PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX,
            PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_INDEX,
            PROJECT_START_BY_ID,
            PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID,
            PROJECT_COMPLETE_BY_INDEX,
            PROJECT_UPDATE_STATUS_BY_ID,
            PROJECT_UPDATE_STATUS_BY_INDEX,
            TASK_CREATE,
            TASK_LIST,
            TASK_GET_BY_ID,
            TASK_GET_BY_INDEX,
            TASK_UPDATE_BY_ID,
            TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID,
            TASK_REMOVE_BY_INDEX,
            TASK_START_BY_ID,
            TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID,
            TASK_COMPLETE_BY_INDEX,
            TASK_UPDATE_STATUS_BY_ID,
            TASK_UPDATE_STATUS_BY_INDEX,
            TASK_BIND_TO_PROJECT,
            TASK_UNBIND_FROM_PROJECT,
            TASK_SHOW_BY_PROJECT,
            TASK_CLEAR,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
